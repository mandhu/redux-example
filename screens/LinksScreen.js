import React from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, Text, View } from 'react-native';

import { connect } from 'react-redux';
import { stopPlayer, startPlayer } from '../store/actions/PlayerAction';

class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Links',
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ alignItems: "center"}}>
          <TouchableOpacity style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Player status {this.props.play ? "true" : "false"}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.props.stopPlayer} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>STOP</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.props.startPlayer} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>START</Text>
          </TouchableOpacity>
        </View>
        
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});


const mapStateToProps = ({ player }) => {
  const { play } = player;

  return {
    play
  }
};

export default connect(mapStateToProps, {
  stopPlayer,
  startPlayer
})(LinksScreen);