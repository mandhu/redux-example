import { PLAYER_START, PLAYER_STOP } from '../types';

export const startPlayer = () => {

    return {
        type: PLAYER_START,
    };
};

export const stopPlayer = () => {
    return {
        type: PLAYER_STOP,
    }
};