import { PLAYER_START, PLAYER_STOP } from '../types';

const INITIAL_STATE = {
    play: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PLAYER_START:
            return {
                ...state,
                play: true
            };

        case PLAYER_STOP:
            return {
                ...state,
                play: false
            };

        default:
            return state;
    }
}